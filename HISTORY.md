Historique du projet
====================

*Date de création*: 2017/06/07


Raisons de mise en place
------------------------
J'utilise de plus en plus Moodle avec mes élèves et je m'énerve de ne pas y
trouver de gestion des versions. De plus, le code HTML produit me fait
littéralement mal aux yeux! C'est pourquoi je me suis décidé à passer tout le
contenu de mon cours sur framagit.

Par contre, je n'attends vraiment pas de pouvoir le partager avec mes
collègues. Bien qu'enseignants en informatique, ils n'en connaissent pas les
outils de base (vim, git, ...). C'est donc vraiment dans un but d'archivage.
Bon, je serai le plus heureux des hommes de pouvoir collaborer avec quelqu'un
sur le contenu de ces cours!




Événements importants
---------------------

### 2017/06/07
Création du dépôt


