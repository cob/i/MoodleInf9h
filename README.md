Contenu Moodle cours informatique 9H
====================================

*date de création* : 2017/06/07


Destinataires du projet
-----------------------
Ce contenu est essentiellement privé. Je le laisse en partage, sans que je
prévoie passer du temps à l'adapter pour autre chose que pour mon propre usage.

Le contenu est destiné à des élèves de cycle d'orientation (13-16 ans) de
classe de développement et d'exigence de base.


Ce que le projet fait
---------------------
Conserver un historique des contenus de mon cours sur Moodle.
Je trouve que les outils de rédactions y sont pitoyables. De plus, il n'assure
pas le suivi des différentes versions.


Prérequis
---------
### Matériels
Il faut disposer d'une plateforme Moodle installée et fonctionnelle.
Je ne garantis vraiment pas que cela fonctionne sur toutes les installations.
J'utilise celle officielle du canton de Fribourg.


### Logiciel
* Moodle...
* git
* gitflow
* vim
* txt2tags
* LaTeX2e


Documentation
-------------

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [CREDITS](CREDITS) liste des contributeurs du projet
4. [FAQ](FAQ.md) "Foire Aux Questions" pour le projet, au format texte
5. [HISTORY](HISTORY.md) histoire du projet
6. [INSTALL](INSTALL.md) instructions de configuration, de compilation et d'installation
7. [LICENSE](LICENSE) termes de la licence
8. [MANIFEST](MANIFEST) liste des fichiers
9. [NEWS](NEWS) dernières nouvelles
10. [TAGS](TAGS) fichier de tags généré automatiquement, pour être utilisé par Emacs ou vi


### Documentation généraliste
#### Git


#### GitFlow


#### LaTex


